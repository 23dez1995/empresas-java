package br.com.ioasys.jhonatas.imdb.config.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.ioasys.jhonatas.imdb.entities.Administrador;
import br.com.ioasys.jhonatas.imdb.repositories.AdministradorRepository;

@Service
public class AutenticacaoService implements UserDetailsService {

	@Autowired
	private AdministradorRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Administrador> admin = repository.findByEmail(username);
		if (admin.isPresent()) {
			return admin.get();
		}
		throw new UsernameNotFoundException("Dados inválidos");
	}

}
