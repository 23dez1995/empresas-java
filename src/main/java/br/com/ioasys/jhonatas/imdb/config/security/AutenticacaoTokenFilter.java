package br.com.ioasys.jhonatas.imdb.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.ioasys.jhonatas.imdb.entities.Administrador;
import br.com.ioasys.jhonatas.imdb.repositories.AdministradorRepository;

public class AutenticacaoTokenFilter extends OncePerRequestFilter {


	private TokenService tokenService;

	private AdministradorRepository administradorRepository;

	public AutenticacaoTokenFilter(TokenService tokenService, AdministradorRepository administradorRepository) {
		this.tokenService = tokenService;
		this.administradorRepository = administradorRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String token = recuparToken(request);
		
		if (tokenService.isTokenValido(token)) {
			autenticarAdministrador(token);
		}
		
		filterChain.doFilter(request, response);
	}

	private void autenticarAdministrador(String token) {
		Administrador admin = administradorRepository.findById(tokenService.getIdAdministrador()).get();
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(admin,null,admin.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	private String recuparToken(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		if (token == null || !token.startsWith("Bearer ") || token.equals("")) {
			return null;
		}
		return token.split(" ")[1].toString();
	}

}
