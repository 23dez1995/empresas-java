package br.com.ioasys.jhonatas.imdb.config.security;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import br.com.ioasys.jhonatas.imdb.entities.Administrador;
import br.com.ioasys.jhonatas.imdb.entities.Usuario;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfigurations {
	
	@Bean
	public  Docket imdbIoasys() {
		
		return new Docket(DocumentationType.SWAGGER_2)  
		          .select()                                  
		          .apis(RequestHandlerSelectors.basePackage("br.com.ioasys.jhonatas.imdb"))              
		          .paths(PathSelectors.ant("/**"))
		          .build()
		          .ignoredParameterTypes(Administrador.class,Usuario.class,UsernamePasswordAuthenticationToken.class)
		          .globalOperationParameters(
	                        Arrays.asList(
	                                new ParameterBuilder()
	                                    .name("Authorization")
	                                    .description("Header para Token JWT")
	                                    .modelRef(new ModelRef("string"))
	                                    .parameterType("header")
	                                    .required(false)
	                                    .build()))
		          .apiInfo(apiInfo()); 
		}
		private ApiInfo apiInfo() {
		    return new ApiInfoBuilder()
		            .title("Documentação Swagger - IMDB(Ioasys)")
		            .description("Desafio Pessoa Desenvolvedora Java")
		            .version("1.0.0")
		            .license("Apache License Version 2.0")
		            .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
		            .build();
		}
}
