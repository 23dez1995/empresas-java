package br.com.ioasys.jhonatas.imdb.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.ioasys.jhonatas.imdb.entities.Administrador;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {

	@Value("${time.token.expiration}")
	private String expiration;

	@Value("${time.token.expiration}")
	private String secret;

	private String tokenAux; 
	
	public String gerarToken(Authentication auth) {
		Administrador admin = (Administrador) auth.getPrincipal();
		Date now = new Date();
		Date expirationDate = new Date(now.getTime() + Long.parseLong(expiration));
		return Jwts.builder()
				.setIssuer("ADMIN")
				.setSubject(admin.getId().toString())
				.setIssuedAt(now)
				.setExpiration(expirationDate)
				.signWith(SignatureAlgorithm.HS256, secret)
				.compact();
	}

	public Boolean isTokenValido(String token) {
		try {
			Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token);
			tokenAux = token;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Integer getIdAdministrador() {
		Claims claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(tokenAux).getBody();
		return Integer.parseInt(claims.getSubject());
	}

}
