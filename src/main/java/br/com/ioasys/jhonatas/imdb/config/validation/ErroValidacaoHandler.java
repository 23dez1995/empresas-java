package br.com.ioasys.jhonatas.imdb.config.validation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErroValidacaoHandler {

	@Autowired
	private MessageSource messageSource;

	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value =  MethodArgumentNotValidException.class)
	public List<ErrorRequest> handle(MethodArgumentNotValidException exception) {
		List<ErrorRequest> listErros = new ArrayList<>();
		exception.getBindingResult().getFieldErrors().forEach(e -> {
			
			String msg = messageSource.getMessage(e, LocaleContextHolder.getLocale());
			ErrorRequest erro = ErrorRequest.builder().campo(e.getField()).erro(msg).build();
			listErros.add(erro);
			
		});
		return listErros;
	}
}
