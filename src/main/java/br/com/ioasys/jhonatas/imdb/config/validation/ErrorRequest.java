package br.com.ioasys.jhonatas.imdb.config.validation;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorRequest {
	private String campo;
	private String erro;
	@Builder.Default
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private final LocalDateTime moment = LocalDateTime.now();
}
