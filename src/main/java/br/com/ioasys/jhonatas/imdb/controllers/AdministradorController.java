package br.com.ioasys.jhonatas.imdb.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.jhonatas.imdb.controllers.dto.PessoaDto;
import br.com.ioasys.jhonatas.imdb.controllers.requests.PessoaRequest;
import br.com.ioasys.jhonatas.imdb.services.AdministradorService;

@RestController
@RequestMapping(value = "administrador/")
public class AdministradorController {

	@Autowired
	private AdministradorService service;

	@PostMapping(value = "cadastrar")
	public ResponseEntity<PessoaDto> cadastrar(@RequestBody @Valid PessoaRequest request) {
		PessoaDto admin = new PessoaDto(service.cadastrar(request));
		return ResponseEntity.status(HttpStatus.CREATED).body(admin);
	}

	@PutMapping(value = "editar")
	public ResponseEntity<PessoaDto> editar(@RequestBody @Valid PessoaRequest request, @RequestParam Integer id) {
		return ResponseEntity.status(HttpStatus.OK).body(new PessoaDto(service.editar(request, id)));
	}

	@DeleteMapping(value = "desativar")
	public ResponseEntity<String> desativar(@RequestParam Integer id) {
		service.desativar(id);
		return ResponseEntity.status(HttpStatus.OK).body("Adminstrador Desativado");
	}

	@GetMapping(value = "listarNaoAtivos")
	public ResponseEntity<Page<PessoaDto>> listarNaoAtivos(
			@PageableDefault(sort = "nome", direction = Direction.ASC) Pageable paginacao) {
		return ResponseEntity.status(HttpStatus.OK).body(service.listarNaoAtivos(paginacao));
	}
}
