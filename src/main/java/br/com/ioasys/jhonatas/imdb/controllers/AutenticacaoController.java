package br.com.ioasys.jhonatas.imdb.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.jhonatas.imdb.config.security.TokenService;
import br.com.ioasys.jhonatas.imdb.controllers.dto.TokenDto;
import br.com.ioasys.jhonatas.imdb.controllers.requests.LoginRequest;

@RestController
@RequestMapping("/auth")

public class AutenticacaoController {

	@Autowired
	private AuthenticationManager authManager;
	
	@Autowired
	private TokenService tokenService;

	@PostMapping
	public ResponseEntity<TokenDto> autenticar(@RequestBody @Valid LoginRequest request) {
		UsernamePasswordAuthenticationToken dadosLogin = request.getAuth();
		try {
			Authentication auth = authManager.authenticate(dadosLogin);
			String token = tokenService.gerarToken(auth);
			return ResponseEntity.status(HttpStatus.OK).body(new TokenDto(token, "Bearer"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
}
