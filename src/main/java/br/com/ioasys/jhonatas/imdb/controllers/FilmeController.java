package br.com.ioasys.jhonatas.imdb.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.jhonatas.imdb.config.security.TokenService;
import br.com.ioasys.jhonatas.imdb.controllers.dto.FilmeDto;
import br.com.ioasys.jhonatas.imdb.controllers.dto.FilmeVotosDto;
import br.com.ioasys.jhonatas.imdb.controllers.requests.FilmeRequest;
import br.com.ioasys.jhonatas.imdb.controllers.requests.VotoRequest;
import br.com.ioasys.jhonatas.imdb.services.FilmeService;

@RestController
@RequestMapping(value = "filme/")
public class FilmeController {
	@Autowired
	private FilmeService service;
	
	@Autowired
	private TokenService tokenService;
	
	@PostMapping(value = "cadastrar")
	public ResponseEntity<FilmeDto> cadastrar(@RequestBody @Valid FilmeRequest request) {
		return ResponseEntity.status(HttpStatus.CREATED).body(new FilmeDto(service.cadastrarFilme(request, tokenService.getIdAdministrador())));
	}

	@PostMapping(value = "votar")
	public ResponseEntity<Void> votar(@RequestBody @Valid VotoRequest voto) {
		service.votar(voto);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@GetMapping(value = "listar")
	public ResponseEntity<Page<FilmeVotosDto>> listar(
			@RequestParam(required = false,defaultValue = "") String diretor,
			@RequestParam(required = false,defaultValue = "") String genero, 
			@RequestParam(required = false,defaultValue = "") String ator,
			@RequestParam(required = false,defaultValue = "") String nome,
			@PageableDefault(sort = "nota", direction = Direction.DESC )Pageable paginacao){
		return ResponseEntity.status(HttpStatus.OK).body(service.listarFilmes(diretor,genero,ator,nome, paginacao));
	}
}
