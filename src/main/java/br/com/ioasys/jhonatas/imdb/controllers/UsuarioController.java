package br.com.ioasys.jhonatas.imdb.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.jhonatas.imdb.controllers.dto.PessoaDto;
import br.com.ioasys.jhonatas.imdb.controllers.requests.PessoaRequest;
import br.com.ioasys.jhonatas.imdb.services.UsuarioService;

@RestController
@RequestMapping(value = "usuario/")
public class UsuarioController {

	@Autowired
	private UsuarioService service;

	@PostMapping(value = "cadastrar")
	public ResponseEntity<PessoaDto> cadastrar(@RequestBody PessoaRequest request) {
		PessoaDto user = new PessoaDto(service.cadastrar(request));
		return ResponseEntity.status(HttpStatus.CREATED).body(user);
	}

	@PutMapping(value = "editar")
	public ResponseEntity<PessoaDto> editar(@RequestBody PessoaRequest request, @RequestParam Integer id) {
		return ResponseEntity.status(HttpStatus.OK).body(new PessoaDto(service.editar(request, id)));
	}

	@DeleteMapping(value = "desativar")
	public ResponseEntity<String> desativar(@RequestParam Integer id) {
		service.desativar(id);
		return ResponseEntity.status(HttpStatus.OK).body("Usuario Desativado");
	}

}
