package br.com.ioasys.jhonatas.imdb.controllers.dto;

import java.util.List;
import java.util.stream.Collectors;

import br.com.ioasys.jhonatas.imdb.entities.Filme;
import br.com.ioasys.jhonatas.imdb.entities.enums.Genero;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmeDto {
	private Integer id;
	private String nome;
	private String administradorResponsavel;
	private String descricao;
	private Genero genero;
	private String diretor;
	private List<String> atores;

	public FilmeDto(Filme filme) {
		this.id = filme.getId();
		this.nome = filme.getNome();
		this.administradorResponsavel = filme.getResponsavel().getNome();
		this.descricao = filme.getDescricao();
		this.genero = filme.getGenero();
		this.diretor = filme.getDiretor().getNome();
		this.atores = filme.getAtores().stream().map(ator -> ator.getNome()).collect(Collectors.toList());
	}
}
