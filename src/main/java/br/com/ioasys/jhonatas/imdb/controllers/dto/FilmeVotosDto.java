package br.com.ioasys.jhonatas.imdb.controllers.dto;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class FilmeVotosDto {
	private Integer id;
	private String nome;
	private String nomeResponsavel;
	private String descricao;
	private Double nota;
	private List<String> atores;
	private String diretor;
	private String genero;

	public FilmeVotosDto(Map<String, Object> rs, List<String> atores) {
		this.id = (Integer) rs.get("id");
		this.nome = (String) rs.get("nome");
		this.nomeResponsavel = (String) rs.get("nomeResponsavel");
		this.descricao = (String) rs.get("descricao");
		this.nota = (Double) rs.get("nota");
		this.atores = atores;
		this.diretor = (String) rs.get("diretor");
		this.genero = (String) rs.get("genero");
	}
}
