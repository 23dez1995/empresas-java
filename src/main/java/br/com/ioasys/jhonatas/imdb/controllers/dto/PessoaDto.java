package br.com.ioasys.jhonatas.imdb.controllers.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.ioasys.jhonatas.imdb.entities.Pessoa;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PessoaDto {
	private Integer id;
	private String nome;
	private String email;
	private String cpf;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private LocalDateTime dataCadastro;
	private Boolean ativo;

	public <T extends Pessoa> PessoaDto(T pessoa) {
		this.id = pessoa.getId();
		this.nome = pessoa.getNome();
		this.cpf = pessoa.getCpf();
		this.dataCadastro = pessoa.getDataCadastro();
		this.email = pessoa.getEmail();
		this.ativo = pessoa.getAtivo();
	}
}
