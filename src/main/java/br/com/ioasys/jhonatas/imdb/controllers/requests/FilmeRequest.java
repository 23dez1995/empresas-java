package br.com.ioasys.jhonatas.imdb.controllers.requests;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.ioasys.jhonatas.imdb.entities.Ator;
import br.com.ioasys.jhonatas.imdb.entities.Diretor;
import br.com.ioasys.jhonatas.imdb.entities.enums.Genero;
import br.com.ioasys.jhonatas.imdb.repositories.AtorRepository;
import br.com.ioasys.jhonatas.imdb.repositories.DiretorRepository;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class FilmeRequest {
	
	@NotNull	@NotEmpty
	private String nome;
	
	@NotNull	@NotEmpty
	private String descricao;
	
	@NotNull
	private Genero genero;
	
	@NotNull 	@NotEmpty
	private String diretor;
	
	@NotNull	@NotEmpty
	private List<String> atores;

	public List<Ator> getListAtores(AtorRepository atorRepository) {
		List<Ator> list = new ArrayList<>();
		atores.forEach(nomeAtor -> {
			Optional<Ator> ator = atorRepository.findByNome(nomeAtor);
			if (ator.isEmpty()) {
				list.add(atorRepository.save(new Ator(nomeAtor)));
			} else {
				list.add(ator.get());
			}

		});
		return list;
	}

	public Diretor getDiretorEntity(DiretorRepository diretorRepository) {
		Optional<Diretor> diretor = diretorRepository.findByNome(this.diretor);
		if (diretor.isEmpty()) {
			return diretorRepository.save(new Diretor(this.diretor));
		} else {
			return diretor.get();
		}
	}
}
