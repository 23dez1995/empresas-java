package br.com.ioasys.jhonatas.imdb.controllers.requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {
	@Email @NotEmpty
	private String email;
	@NotEmpty @Length(min = 10,max = 20)
	private String senha;
	
	public UsernamePasswordAuthenticationToken getAuth() {
		return new UsernamePasswordAuthenticationToken(this.email,this.senha);
	}
}
