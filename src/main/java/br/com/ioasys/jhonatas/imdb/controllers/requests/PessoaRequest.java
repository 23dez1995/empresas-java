package br.com.ioasys.jhonatas.imdb.controllers.requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PessoaRequest {
	
	@NotNull @NotEmpty
	private String nome;
	@CPF @NotEmpty @NotNull
	private String cpf;
	@NotEmpty @NotNull @Length(min = 10,max = 20)
	private String senha;
	@Email @NotEmpty
	private String email;
	
	public String getSenha(){
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(this.senha);
	}
}
