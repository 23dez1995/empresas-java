package br.com.ioasys.jhonatas.imdb.controllers.requests;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VotoRequest {
	@NotNull
	private Integer idFilme;
	@NotNull @Min(0) @Max(5)
	private  Double voto;
	@NotNull
	private Integer usuarioId;
}
