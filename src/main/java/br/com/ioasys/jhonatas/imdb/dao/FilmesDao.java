package br.com.ioasys.jhonatas.imdb.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.ioasys.jhonatas.imdb.controllers.dto.FilmeVotosDto;

@Repository
public class FilmesDao {

	@Autowired
	private EntityManager em;

	@SuppressWarnings("deprecation")
	public Page<FilmeVotosDto> buscarFilmes(String diretor, String nome, String ator, Pageable paginacao) {
		List<FilmeVotosDto> listFilmeVotosDto = new ArrayList<>();

		Query query = em.createNativeQuery(sqlBuscaFilmes());
		query.setParameter("diretor", diretor);
		query.setParameter("nome", nome);
		query.setParameter("ator", ator);
		query.unwrap(NativeQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

		@SuppressWarnings("unchecked")
		List<Map<String, Object>> list = query.getResultList();
		list.forEach(map -> {
			Integer id = (Integer) map.get("id");
			listFilmeVotosDto.add(new FilmeVotosDto(map, buscarNomeAtores(id)));
		});
		
		return new PageImpl<FilmeVotosDto>(listFilmeVotosDto, paginacao, listFilmeVotosDto.size());
	}

	private List<String> buscarNomeAtores(Integer id) {
		Query query = em.createNativeQuery(sqlBuscaNomeAtores());
		query.setParameter("idFilme", id);
		@SuppressWarnings("unchecked")
		List<String> nomes = query.getResultList();
		return nomes;
	}

	private String sqlBuscaFilmes() {
		StringBuilder sb = new StringBuilder();
		sb.append(" select	DISTINCT ");
		sb.append(" f.id as id,");
		sb.append(" f.nome as nome,");
		sb.append(" a3.nome as nomeResponsavel,");
		sb.append(" f.descricao as descricao,");
		sb.append(" (SELECT	AVG(vf2.voto) from votos_filme vf2 where vf2.filme_id = f.id) as nota,");
		sb.append(" d.nome as diretor,");
		sb.append(" f.genero as genero");
		sb.append(" from filme f");
		sb.append(" join diretor d on f.diretor_id = d.id");
		sb.append(" join filme_atores fa on	fa.filme_id = f.id");
		sb.append(" join ator a on	a.id = fa.atores_id");
		sb.append(" join votos_filme vf on	vf.filme_id = f.id");
		sb.append(" join administrador a3 on a3.id = f.responsavel_id");
		sb.append(" where 1=1 ");
		sb.append(" and upper(f.nome) like upper(:nome)");
		sb.append(" AND UPPER(d.nome) like upper(:diretor)");
		sb.append(" and a.nome in (	SELECT	a2.nome	from ator a2 where	upper(a2.nome) like upper(:ator))");
		return sb.toString();
	}

	private String sqlBuscaNomeAtores() {
		StringBuilder sb = new StringBuilder();
		sb.append(" select a.nome from ator a join filme_atores fa on fa.atores_id = a.id where fa.filme_id = :idFilme ");
		return sb.toString();
	}

}
