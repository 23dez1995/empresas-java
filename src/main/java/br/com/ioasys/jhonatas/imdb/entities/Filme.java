package br.com.ioasys.jhonatas.imdb.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import br.com.ioasys.jhonatas.imdb.controllers.requests.FilmeRequest;
import br.com.ioasys.jhonatas.imdb.entities.enums.Genero;
import br.com.ioasys.jhonatas.imdb.repositories.AtorRepository;
import br.com.ioasys.jhonatas.imdb.repositories.DiretorRepository;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
public class Filme {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String nome;

	@ManyToOne
	private Administrador responsavel;

	private String descricao;
	@ManyToMany
	private List<Ator> atores;
	@Enumerated(EnumType.STRING)
	private Genero genero;
	@ManyToOne
	private Diretor diretor;

	public Filme(String nome, Administrador responsavel, FilmeRequest request,AtorRepository atorRepository, DiretorRepository diretorRepository) {
		this.nome = nome;
		this.responsavel = responsavel;
		this.descricao = request.getDescricao();
		this.genero = request.getGenero();
		this.diretor = request.getDiretorEntity(diretorRepository);
		this.atores = request.getListAtores(atorRepository);
	}
}
