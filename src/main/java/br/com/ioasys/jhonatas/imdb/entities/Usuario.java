package br.com.ioasys.jhonatas.imdb.entities;

import javax.persistence.Entity;

import br.com.ioasys.jhonatas.imdb.controllers.requests.PessoaRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Usuario extends Pessoa{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Usuario(PessoaRequest request) {
		this.nome = request.getNome();
		this.cpf = request.getCpf();
		this.ativo = true;
		this.senha = request.getSenha();
		this.email = request.getEmail();
	}
}
