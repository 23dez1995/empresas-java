package br.com.ioasys.jhonatas.imdb.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VotosFilme {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	private Filme filme;

	@Min(value = 0)
	@Max(value = 5)
	private Double voto;

	@ManyToOne
	private Usuario usuario;

	public VotosFilme(Filme filme, Double voto, Usuario usuario) {
		this.filme = filme;
		this.voto = voto;
		this.usuario = usuario;
	}
}
