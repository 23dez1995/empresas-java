package br.com.ioasys.jhonatas.imdb.entities.enums;

public enum Genero {
	ACAO, TERROR, DRAMA, ROMANCE, COMEDIA;
}
