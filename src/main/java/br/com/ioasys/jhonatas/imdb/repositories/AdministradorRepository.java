package br.com.ioasys.jhonatas.imdb.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ioasys.jhonatas.imdb.entities.Administrador;

public interface AdministradorRepository extends JpaRepository<Administrador, Integer> {

	Page<Administrador> findByAtivo(boolean b, Pageable paginacao);

	Optional<Administrador> findByEmail(String username);

}
