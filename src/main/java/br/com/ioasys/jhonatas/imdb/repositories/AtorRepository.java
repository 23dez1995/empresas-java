package br.com.ioasys.jhonatas.imdb.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ioasys.jhonatas.imdb.entities.Ator;

@Repository
public interface AtorRepository extends JpaRepository<Ator, Integer> {

	Optional<Ator> findByNome(String nomeAtor);

}
