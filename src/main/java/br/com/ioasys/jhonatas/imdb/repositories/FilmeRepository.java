package br.com.ioasys.jhonatas.imdb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ioasys.jhonatas.imdb.entities.Filme;

@Repository
public interface FilmeRepository extends JpaRepository<Filme, Integer> {
}
