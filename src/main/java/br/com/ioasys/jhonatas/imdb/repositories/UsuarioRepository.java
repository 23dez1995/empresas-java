package br.com.ioasys.jhonatas.imdb.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ioasys.jhonatas.imdb.entities.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
