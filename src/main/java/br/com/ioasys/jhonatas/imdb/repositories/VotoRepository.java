package br.com.ioasys.jhonatas.imdb.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ioasys.jhonatas.imdb.entities.Filme;
import br.com.ioasys.jhonatas.imdb.entities.VotosFilme;

@Repository
public interface VotoRepository extends JpaRepository<VotosFilme, Integer> {

	List<VotosFilme> findAllByFilme(Filme filme);

}
