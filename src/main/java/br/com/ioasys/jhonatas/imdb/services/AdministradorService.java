package br.com.ioasys.jhonatas.imdb.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.ioasys.jhonatas.imdb.controllers.dto.PessoaDto;
import br.com.ioasys.jhonatas.imdb.controllers.requests.PessoaRequest;
import br.com.ioasys.jhonatas.imdb.entities.Administrador;
import br.com.ioasys.jhonatas.imdb.repositories.AdministradorRepository;

@Service
public class AdministradorService {
	@Autowired
	private AdministradorRepository repository;

	public Administrador cadastrar(PessoaRequest request) {
		return repository.save(new Administrador(request));
	}

	public Administrador editar(PessoaRequest request, Integer id) {
		Optional<Administrador> admin = repository.findById(id);
		if (admin.isPresent()) {
			admin.get().setNome(request.getNome());
			admin.get().setCpf(request.getCpf());
			admin.get().setSenha(request.getSenha());
			return repository.save(admin.get());
		}
		return null;
	}

	public void desativar(Integer id) {
		Optional<Administrador> admin = repository.findById(id);
		if (admin.isPresent()) {
			admin.get().setAtivo(false);
			repository.save(admin.get());
		}
	}

	public Page<PessoaDto> listarNaoAtivos(Pageable paginacao) {
		Page<PessoaDto> administrador = repository.findByAtivo(false, paginacao).map(x -> new PessoaDto(x));
		return administrador;
	}
}
