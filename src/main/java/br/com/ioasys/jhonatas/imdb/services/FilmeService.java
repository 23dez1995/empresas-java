package br.com.ioasys.jhonatas.imdb.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.ioasys.jhonatas.imdb.controllers.dto.FilmeVotosDto;
import br.com.ioasys.jhonatas.imdb.controllers.requests.FilmeRequest;
import br.com.ioasys.jhonatas.imdb.controllers.requests.VotoRequest;
import br.com.ioasys.jhonatas.imdb.dao.FilmesDao;
import br.com.ioasys.jhonatas.imdb.entities.Administrador;
import br.com.ioasys.jhonatas.imdb.entities.Filme;
import br.com.ioasys.jhonatas.imdb.entities.Usuario;
import br.com.ioasys.jhonatas.imdb.entities.VotosFilme;
import br.com.ioasys.jhonatas.imdb.repositories.AdministradorRepository;
import br.com.ioasys.jhonatas.imdb.repositories.AtorRepository;
import br.com.ioasys.jhonatas.imdb.repositories.DiretorRepository;
import br.com.ioasys.jhonatas.imdb.repositories.FilmeRepository;
import br.com.ioasys.jhonatas.imdb.repositories.UsuarioRepository;
import br.com.ioasys.jhonatas.imdb.repositories.VotoRepository;

@Service
public class FilmeService {
	@Autowired
	private FilmeRepository filmeRepository;

	@Autowired
	private VotoRepository votoRepository;

	@Autowired
	private AdministradorRepository administradorRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private DiretorRepository diretorRepository;

	@Autowired
	private AtorRepository atorRepository;
	
	@Autowired
	private FilmesDao dao;

	public Filme cadastrarFilme(FilmeRequest request, Integer administradorId) {
		Optional<Administrador> admin = administradorRepository.findById(administradorId);
		if (admin.isPresent()) {
			return filmeRepository.save(
								new Filme(	
								request.getNome(),
								admin.get(), 
								request,
								atorRepository, 
								diretorRepository));
		}
		return null;
	}

	public void votar(VotoRequest voto) {
		Optional<Filme> filme = filmeRepository.findById(voto.getIdFilme());
		Optional<Usuario> usuario = usuarioRepository.findById(voto.getUsuarioId());
		votoRepository.save(new VotosFilme(filme.get(), voto.getVoto(), usuario.get()));
	}

	public Page<FilmeVotosDto> listarFilmes(String diretor, String genero, String ator, String nome, Pageable paginacao) {
		Page<FilmeVotosDto> filmes = dao.buscarFilmes(addRecursoLike(diretor),addRecursoLike(ator),addRecursoLike(nome),paginacao);
		return filmes;
	}
	
	private String addRecursoLike(String campo) {
		return "%"+campo+"%";
	}
}
