package br.com.ioasys.jhonatas.imdb.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ioasys.jhonatas.imdb.controllers.requests.PessoaRequest;
import br.com.ioasys.jhonatas.imdb.entities.Usuario;
import br.com.ioasys.jhonatas.imdb.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository repository;

	public Usuario cadastrar(PessoaRequest request) {
		return repository.save(new Usuario(request));
	}

	public Usuario editar(PessoaRequest request, Integer id) {
		Optional<Usuario> user = repository.findById(id);
		if (user.isPresent()) {
			user.get().setNome(request.getNome());
			user.get().setCpf(request.getCpf());
			user.get().setSenha(request.getSenha());
			user.get().setEmail(request.getEmail());
			return repository.save(user.get());
		}
		return null;
	}

	public void desativar(Integer id) {
		Optional<Usuario> user = repository.findById(id);
		if (user.isPresent()) {
			user.get().setAtivo(false);
			repository.save(user.get());
		}
	}

}
