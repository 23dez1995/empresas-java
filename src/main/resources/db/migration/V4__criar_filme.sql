CREATE TABLE `filme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(10000) DEFAULT NULL,
  `genero` varchar(255) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `diretor_id` int(11) DEFAULT NULL,
  `responsavel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKs77pnrgth379pum02tyqls55c` (`diretor_id`),
  KEY `FKpmn4p1pcj7j2sclx6sa00g73a` (`responsavel_id`),
  CONSTRAINT `FKpmn4p1pcj7j2sclx6sa00g73a` FOREIGN KEY (`responsavel_id`) REFERENCES `administrador` (`id`),
  CONSTRAINT `FKs77pnrgth379pum02tyqls55c` FOREIGN KEY (`diretor_id`) REFERENCES `diretor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;