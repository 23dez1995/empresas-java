CREATE TABLE `filme_atores` (
  `filme_id` int(11) NOT NULL,
  `atores_id` int(11) NOT NULL,
  KEY `FKrhxhb3w8xilupj7mxujla2axx` (`atores_id`),
  KEY `FK56t7695moolm213t5ura58uo7` (`filme_id`),
  CONSTRAINT `FK56t7695moolm213t5ura58uo7` FOREIGN KEY (`filme_id`) REFERENCES `filme` (`id`),
  CONSTRAINT `FKrhxhb3w8xilupj7mxujla2axx` FOREIGN KEY (`atores_id`) REFERENCES `ator` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;