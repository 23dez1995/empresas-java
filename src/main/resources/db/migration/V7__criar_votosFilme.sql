CREATE TABLE `votos_filme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voto` double DEFAULT NULL,
  `filme_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3pwh3r1sqta4p6bx2lcopsr57` (`filme_id`),
  KEY `FKoacl4d7004c1yndgxj333730p` (`usuario_id`),
  CONSTRAINT `FK3pwh3r1sqta4p6bx2lcopsr57` FOREIGN KEY (`filme_id`) REFERENCES `filme` (`id`),
  CONSTRAINT `FKoacl4d7004c1yndgxj333730p` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;