INSERT INTO administrador(id, ativo, cpf, data_cadastro, nome, senha, email) VALUES(null, 1, '40122107004', '2021-07-22 21:24:27.0', 'Ioasys', '$2a$10$GQfC.1ryzOwyOMDQ8Af7m.60bbypUeHAd6oBYPeCYwcnfre9zP5by', 'teste@ioasys.com.br');
INSERT INTO administrador(id, ativo, cpf, data_cadastro, nome, senha, email) VALUES(null, 1, '83529127043', '2021-07-22 22:24:02.580891', 'Jhonatas Alves', '$2a$10$gshVqdZPPpNfxWU16xxpreq686gcZlYN50UFbf5i4LZmmfpxbYzQK', 'jhonatas@gmail.com');

INSERT INTO usuario(id, ativo, cpf, data_cadastro, nome, senha, email) VALUES(null, 1, '72168339023', '2021-07-22 21:27:27.289994', 'Joao Carlos', '$2a$10$2.Oi5LealWBq23lb9xBP5u7njATP1C8MZnYkiRPvKWfw1MTnHlesO', 'joao@gmail.com');
INSERT INTO usuario(id, ativo, cpf, data_cadastro, nome, senha, email) VALUES(null, 1, '15104808033', '2021-07-22 22:27:13.54376', 'Maria Carla', '$2a$10$Htar/YzmWOjaSJwlXmDqB.8AqJie/tw4Tyem3hDu0TOOyJ.EaVVKe', 'maria@hotmail.com');
INSERT INTO usuario(id, ativo, cpf, data_cadastro, nome, senha, email) VALUES(null, 1, '23675985091', '2021-07-22 22:29:03.077898', 'Eduardo Braga', '$2a$10$lutQO38./RBc0qwJGU2eZOr2G5indD/LgnbYrP1K..D/Ic/Vyyyi6', 'dudu@yahoo.com');

INSERT INTO ator(id, nome) VALUES(null, 'Robert Downey Jr');
INSERT INTO ator(id, nome) VALUES(null, 'Scarlett Johansson');
INSERT INTO ator(id, nome) VALUES(null, 'Don Cheadle');
INSERT INTO ator(id, nome) VALUES(null, 'Gwyneth Paltrow');
INSERT INTO ator(id, nome) VALUES(null, 'Chris Hemsworth');
INSERT INTO ator(id, nome) VALUES(null, 'Chris Evans');
INSERT INTO ator(id, nome) VALUES(null, 'Mark Ruffalo');
INSERT INTO ator(id, nome) VALUES(null, 'Jeremy Renner');
INSERT INTO ator(id, nome) VALUES(null, 'Tom Hiddleston');
INSERT INTO ator(id, nome) VALUES(null, 'Zachary Levi');
INSERT INTO ator(id, nome) VALUES(null, 'Idris Elba');

INSERT INTO diretor(id, nome) VALUES(null, 'Jon Favreau');
INSERT INTO diretor(id, nome) VALUES(null, 'Joss Whedon');
INSERT INTO diretor(id, nome) VALUES(null, 'Alan Taylor');

INSERT INTO filme(id, descricao, genero, nome, diretor_id, responsavel_id) VALUES(null, 'Loki, o irmão de Thor, ganha acesso ao poder ilimitado do cubo cósmico ao roubá-lo de dentro das instalações da S.H.I.E.L.D. Nick Fury, o diretor desta agência internacional que mantém a paz, logo reúne os únicos super-heróis que serão capazes de defender a Terra de ameaças sem precedentes. Homem de Ferro, Capitão América, Hulk, Thor, Viúva Negra e Gavião Arqueiro formam o time dos sonhos de Fury, mas eles precisam aprender a colocar os egos de lado e agir como um grupo em prol da humanidade.', 'ACAO', 'Marvel''s The Avengers: Os Vingadores', 2, 1);
INSERT INTO filme(id, descricao, genero, nome, diretor_id, responsavel_id) VALUES(null, 'Tony Stark é um industrial bilionário e inventor brilhante que realiza testes bélicos no exterior, mas é sequestrado por terroristas que o forçam a construir uma arma devastadora. Em vez disso, ele constrói uma armadura blindada e enfrenta seus sequestradores. Ao voltar para os EUA, Stark aprimora a armadura e a utiliza para combater o crime.', 'ACAO', 'Iron Man 1', 1, 1);
INSERT INTO filme(id, descricao, genero, nome, diretor_id, responsavel_id) VALUES(null, 'Thor precisa contar com a ajuda de seus companheiros e até de seu traiçoeiro irmão Loki em um plano audacioso para salvar o universo. Entretanto, os caminhos de Thor se cruzam com Jane Foster e, dessa vez, a vida dela está realmente em perigo.', 'ACAO', 'Thor: The Dark World', 3, 1);

INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 2.5, 1, 1);
INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 3.8, 2, 1);
INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 5.0, 3, 1);
INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 4.0, 1, 2);
INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 4.5, 2, 2);
INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 1.5, 3, 2);
INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 2.5, 1, 3);
INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 2.7, 2, 3);
INSERT INTO votos_filme(id, voto, filme_id, usuario_id) VALUES(null, 3.0, 3, 3);

INSERT INTO filme_atores(filme_id, atores_id) VALUES(1, 1);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(1, 2);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(1, 3);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(1, 4);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(1, 5);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(1, 6);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(1, 7);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(1, 8);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(2, 1);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(2, 2);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(2, 3);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(2, 4);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(3, 5);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(3, 7);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(3, 8);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(3, 9);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(3, 10);
INSERT INTO filme_atores(filme_id, atores_id) VALUES(3, 11);
